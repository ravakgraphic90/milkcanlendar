import { AppRegistry, Platform } from 'react-native';
import App from './App';
import Animations from './animations';

AppRegistry.registerComponent('MilkCalendar', () => Animations);

if (Platform.OS === 'web') {
  const rootTag = document.getElementById('root') || document.getElementById('main');
  AppRegistry.runApplication('MilkCalendar', { rootTag });
}
