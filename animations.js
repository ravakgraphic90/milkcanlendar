import React from 'react'
import { View, Text } from 'react-native'
import LottieView from 'lottie-react-native'
const Animations = () => {
    return (
        <View style={{flex:1,backgroundColor:'cyan'}}>
            <View style={{flex:1}}>
            <LottieView source={require('./assets/animations/lottie.json')} autoPlay loop
            />
            </View>
            <View style={{flex:1}}>
            <LottieView source={require('./assets/animations/lottie2.json')} autoPlay loop
            />
            </View>
            <View style={{flex:1}}>
            <LottieView source={require('./assets/animations/lottie3.json')} autoPlay loop
            />
            </View>
        </View>
    )
}

export default Animations
